

# Student


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**id** | **Long** |  |  [optional] |
|**firstname** | **String** |  |  [optional] |
|**lastname** | **String** |  |  [optional] |



